CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE users
(
    user_id uuid DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL CHECK(length(name)  >= 2),
    surname VARCHAR(255) NOT NULL CHECK(length(surname) >= 2),
    username VARCHAR(40) NOT NULL UNIQUE CHECK(username ~* '^[\\p{L}&?]{7,40}$'),
    email VARCHAR(255) NOT NULL UNIQUE CHECK(email ~* '^[a-z0-9._%+-]{10,}@[a-z0-9.-]+\.[a-z]{2,}$'),
    tel_number VARCHAR(50) NOT NULL UNIQUE CHECK((tel_number ~* '^(\+[\d]{1,3}\s[\d]{1,3}\s[\d]{1,2}\s[\d]{1,2}\s[\d]{1,2}\s[\d]{1,2})$') AND length(tel_number) >= 5),
    id_number VARCHAR(15) NOT NULL UNIQUE CHECK(id_number ~ '^\d{11,15}$'),
    password VARCHAR(250) NOT NULL CHECK(length(id_number) >= 7),
    date_created VARCHAR(100) NOT NULL,
    ip_address INET NOT NULL,
    verified  BOOL NOT NULL DEFAULT FALSE,
    auto_logout_time  SMALLINT NOT NULL  DEFAULT 30
);

CREATE OR REPLACE FUNCTION Hash(value VARCHAR, salt BYTEA)
    RETURNS VARCHAR AS $$
DECLARE
    hashed_password VARCHAR;
BEGIN
    IF length(value) < 7 THEN
        RAISE EXCEPTION 'Password must be at least 7 characters long';
    END IF;

    IF length(salt) < 100 THEN
        RAISE EXCEPTION 'Salt must be at least 100 bytes long';
    END IF;

    hashed_password := Hash(value, '$argon2id$v=19$m=10000,t=20,p=3$' || encode(salt, 'hex') || '$100');
    RETURN hashed_password;
END;
$$ LANGUAGE plpgsql;
