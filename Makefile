# Makefile

.PHONY: build kill run

NETWORK_NAME = authNetwork
REDIS_CONTAINER_NAME = redis
POSTGRES_CONTAINER_NAME = authPostgres
POSTGRES_USERNAME = david
POSTGRES_PASSWORD = david
POSTGRES_DB = auth
RABBITMQ_CONTAINER_NAME = rabbitMQ
RABBITMQ_USERNAME = david
RABBITMQ_PASSWORD = david

tls:
	go run cmd/main.go -cert /Users/davitdarsalia/Desktop/cert.pem  -key /Users/davitdarsalia/Desktop/key.pem

run: kill build dbUp

up:
	docker network create $(NETWORK_NAME) || true
	docker pull postgres:latest
	docker pull rabbitmq:3-management
	docker pull redis:latest
	docker run --name $(POSTGRES_CONTAINER_NAME) -e POSTGRES_USER=$(POSTGRES_USERNAME) -e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) -e POSTGRES_DB=$(POSTGRES_DB) -p 5432:5432 --network $(NETWORK_NAME) -d postgres:latest || docker start $(POSTGRES_CONTAINER_NAME)
	docker run --name $(RABBITMQ_CONTAINER_NAME) -e RABBITMQ_DEFAULT_USER=$(RABBITMQ_USERNAME) -e RABBITMQ_DEFAULT_PASS=$(RABBITMQ_PASSWORD) -p 5672:5672 -p 15672:15672 --network $(NETWORK_NAME) -d rabbitmq:3-management || docker start $(RABBITMQ_CONTAINER_NAME)
	docker run --name $(REDIS_CONTAINER_NAME) -p 6379:6379 --network $(NETWORK_NAME) -d redis:latest || docker start $(REDIS_CONTAINER_NAME)

down:
	docker container stop $(POSTGRES_CONTAINER_NAME) || true
	docker container rm $(POSTGRES_CONTAINER_NAME) || true
	docker container stop $(RABBITMQ_CONTAINER_NAME) || true
	docker container rm $(RABBITMQ_CONTAINER_NAME) || true
	docker container stop $(REDIS_CONTAINER_NAME) || true
	docker container rm $(REDIS_CONTAINER_NAME) || true
	docker network rm $(NETWORK_NAME) || true

dbUp:
	migrate -path ./migrations -database 'postgres://david:david@localhost:5432/auth?sslmode=disable' up

dbDown:
	migrate -path ./migrations -database 'postgres://david:david@localhost:5432/auth?sslmode=disable' down