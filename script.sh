# Initialization of migrations
migrate create -ext sql -dir ./schema -seq init
# Migration UP
migrate -path ./migrations -database 'postgres://david:david@localhost:5435/postgres?sslmode=disable' up
# Use this
migrate -path ./migrations -database 'postgres://david:david@localhost:5435/auth?sslmode=disable' up

# Start refreshable server
gin --immediate --all run cmd/main.go
# Generates mocks by Mockery library
go:generate mockery --name=Authorizer
# Run
docker compose down && docker compose build && docker compose up -d
# Upgrade helper tools
go get -u gitlab.com/microservices3525312/tools/microhelpers@0.0.14
# Lint
golangci-lint run --skip-dirs=vendor,testdata ./...