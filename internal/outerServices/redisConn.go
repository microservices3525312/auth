package outerServices

import (
	"time"

	"github.com/go-redis/redis/v8"
)

func RedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:         "localhost:6379",
		DialTimeout:  5 * time.Second,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		PoolSize:     15,
		MinIdleConns: 10,
	})
}
