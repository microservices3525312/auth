package entities

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"fmt"
	"net/http"
	"time"
)

type Server struct {
	httpServer *http.Server
}

func (s *Server) Run(port string, h http.Handler) error {
	s.httpServer = &http.Server{
		Addr:           fmt.Sprintf(":%s", port),
		Handler:        h,
		MaxHeaderBytes: 1 << 20,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		TLSConfig: &tls.Config{
			Rand:                        rand.Reader,
			ClientAuth:                  tls.RequireAndVerifyClientCert,
			ClientSessionCache:          tls.NewLRUClientSessionCache(1000),
			MinVersion:                  tls.VersionTLS13,
			DynamicRecordSizingDisabled: false,
			Renegotiation:               0,
		},
	}

	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
