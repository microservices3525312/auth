package entities

type User struct {
	Name              string `json:"name" binding:"required" validate:"required,min=2,max=255"`
	Surname           string `json:"surname" binding:"required" validate:"required,min=2,max=255"`
	UserName          string `json:"username" binding:"required" validate:"required,min=7,max=40"`
	Email             string `json:"email" binding:"required" validate:"required,min=10,max=255,email"`
	TelNumber         string `json:"tel_number" binding:"required" validate:"required,min=5,max=50,e164"`
	IDNumber          string `json:"id_number" binding:"required" validate:"required,min=11,max=11"`
	Password          string `json:"password" binding:"required" validate:"required,min=7,max=200"`
	AutoLogoutTime    uint16 `json:"auto_logout_time"`
	UserIdentifierKey string `json:"user_identifier_key"`
	DateCreated       string `json:"date_created"`
	IPAddress         string `json:"ip_address"`
}

type UserInput struct {
	Email             string `json:"email" binding:"required" validate:"required,min=10,max=255,email"`
	Password          string `json:"password" binding:"required"  validate:"required,min=7,max=200"`
	IDNumber          string `json:"id_number" binding:"required" validate:"required,min=11,max=11"`
	UserIdentifierKey string `json:"user_identifier_key"`
}

type RecoverPasswordInput struct {
	Email       string `json:"email" binding:"required" validate:"required,min=10,max=255,email"`
	IDNumber    string `json:"id_number" binding:"required" validate:"required,min=11,max=11"`
	TelNumber   string `json:"tel_number" binding:"required" validate:"required,min=5,max=50,e164"`
	NewPassword string `json:"new_password" binding:"required" validate:"required,min=7,max=200"`
}

type ResetPasswordInput struct {
	Code          string `json:"code" binding:"required" validate:"required,min=8,max=29"`
	NewPassword   string `json:"new_password" binding:"required" validate:"required,min=7,max=200"`
	Email         string `json:"email" binding:"required" validate:"required,min=10,max=255,email"`
	TelNumber     string `json:"tel_number" binding:"required" validate:"required,min=5,max=50,e164"`
	Id            string `json:"id"`
	IdentifierKey string `json:"identifier_key"`
}

type AuthenticatedUserResponse struct {
	UserID                string `json:"user_id"`
	AccessToken           string `json:"access_token"`
	AccessTokenExpiresAt  string `json:"access_token_exp"`
	RefreshToken          string `json:"refresh_token"`
	RefreshTokenExpiresAt string `json:"refresh_token_exp"`
}

type UserMetaInfo struct {
	Password string `json:"password"`
	UserID   string `json:"user_id"`
}

type RefreshLoginEntity struct {
	RefreshHeader string `json:"refresh_header"`
	UserID        string `json:"user_id"`
}
