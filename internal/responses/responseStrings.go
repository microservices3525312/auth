package responses

/* Success */

const (
	CreateUserSuccessMessage        = `[Create User Handler] - User Created Successfully`
	LoggedInUserSuccessMessage      = `[Log In User Handler] - User Logged In Successfully`
	RecoveredPasswordSuccessMessage = `[Recovered Password Handler] - Code Sent To Your Email`
	ResetPasswordSuccessMessage     = `[Reset Password Handler] - Your Password Successfully Reset`
	RefreshLoginSuccessMessage      = `[Refresh Login Handler] - Successful Refresh Login`
)

/* Errors */

const (
	CreateUserErrorMessage     = `[Create User Handler] - User Already Exists`
	LogInUserErrorMessage      = `[Log In User Handler] - User Not Found`
	BadRequestErrorMessage     = `[Generic Message] - Some Of Your Credentials Are Missing Or Incorrect`
	RequestEntityTooLargeError = `[Middleware Generic Message] - Request Body Is Too Large: x > 300KB`
	RefreshLoginError          = `[Refresh Login Handler] - Invalid Token Or UserID, No Refresh Login Possible`
)

/* Constants */

const (
	ValidationFailedErrorMessage = `verifications failed for fields`
)
