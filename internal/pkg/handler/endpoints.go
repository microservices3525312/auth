package handler

import (
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/microservices3525312/auth/internal/entities"
	"gitlab.com/microservices3525312/auth/internal/responses"
	"gitlab.com/microservices3525312/tools/microhelpers/microHelpers"
)

func (h *Handler) createUser(c *gin.Context) {
	var u entities.User

	{
		validationChain(c, h.validator, h.policyManager, &u)
	}

	resp, err := h.service.CreateUser(&u)

	{
		if err != nil {
			var statusCode int

			if strings.Contains(err.Error(), responses.ValidationFailedErrorMessage) {
				statusCode = http.StatusNotAcceptable
				newErrorResponse(c, statusCode, err.Error())
			} else {
				statusCode = http.StatusConflict
				newErrorResponse(c, statusCode, err.Error())
			}

			return
		}
	}

	{
		c.JSON(http.StatusCreated, responses.CreateUserResponse{
			UserID: resp.UserID,
			CreateUserGenericMessage: responses.CreateUserGenericMessage{
				StatusCode: http.StatusCreated,
				Message:    responses.CreateUserSuccessMessage,
			},
			AuthenticatedUserResponse: resp,
		})
	}

	{
		err = c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (h *Handler) loginUser(c *gin.Context) {
	var u entities.UserInput

	validationChain(c, h.validator, h.policyManager, &u)

	//{
	//	if err := c.BindJSON(&u); err != nil {
	//		newErrorResponse(c, http.StatusBadRequest, responses.BadRequestErrorMessage)
	//		return
	//	}
	//
	//	err := helperTools.XSSInput(&u, h.policyManager)
	//	if err != nil {
	//		newErrorResponse(c, http.StatusUnprocessableEntity, err.Error())
	//		return
	//	}
	//}
	//
	//{
	//	if err := h.validator.Struct(&u); err != nil {
	//		newErrorResponse(c, http.StatusUnprocessableEntity, fmt.Sprintf("%s", helperTools.GenerateValidationStruct(err)))
	//		return
	//	}
	//}

	u.UserIdentifierKey = c.GetHeader(os.Getenv("IDENTITY_KEY_HEADER"))
	resp, err := h.service.LoginUser(u)

	if err != nil {
		var statusCode int

		if strings.Contains(err.Error(), responses.ValidationFailedErrorMessage) {
			statusCode = http.StatusNotAcceptable
			newErrorResponse(c, statusCode, err.Error())
		} else {
			statusCode = http.StatusNotFound
			newErrorResponse(c, statusCode, responses.LogInUserErrorMessage)
		}
		return
	}

	{
		c.JSON(http.StatusOK, responses.LoginUserResponse{
			UserID: resp.UserID,
			LoginUserGenericMessage: responses.LoginUserGenericMessage{
				StatusCode: http.StatusOK,
				Message:    responses.LoggedInUserSuccessMessage,
			},
			AuthenticatedUserResponse: resp,
		})
	}

	{
		err = c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (h *Handler) refreshLogin(c *gin.Context) {
	var r entities.RefreshLoginEntity

	{
		r.RefreshHeader = c.GetHeader(os.Getenv("REFRESH_LOGIN_HEADER"))
		r.UserID = c.GetHeader(os.Getenv("USER_ID_HEADER"))
	}

	{
		if err := helperTools.XSSInput(&r, h.policyManager); err != nil {
			newErrorResponse(c, http.StatusUnprocessableEntity, err.Error())
			return
		}
	}

	resp, err := h.service.RefreshLogin(r)

	{
		if err != nil {
			newErrorResponse(c, http.StatusNotAcceptable, responses.RefreshLoginError)
			return
		}
	}

	{
		c.JSON(http.StatusOK, responses.LoginUserResponse{
			UserID: resp.UserID,
			LoginUserGenericMessage: responses.LoginUserGenericMessage{
				StatusCode: http.StatusOK,
				Message:    responses.RefreshLoginSuccessMessage,
			},
			AuthenticatedUserResponse: resp,
		})
	}

}

func (h *Handler) requestResetPassword(c *gin.Context) {
	var u entities.RecoverPasswordInput

	{
		validationChain(c, h.validator, h.policyManager, &u)
	}

	err := h.service.RequestPasswordRecover(&u)

	{
		if err != nil {
			var statusCode int

			if strings.Contains(err.Error(), responses.ValidationFailedErrorMessage) {
				statusCode = http.StatusNotAcceptable
			} else {
				statusCode = http.StatusNotFound
			}

			newErrorResponse(c, statusCode, err.Error())
			return
		}
	}

	{
		c.JSON(http.StatusOK, responses.RecoveredPasswordResponse{
			StatusCode: http.StatusOK,
			Message:    responses.RecoveredPasswordSuccessMessage,
		})
	}

	{
		err = c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (h *Handler) resetPassword(c *gin.Context) {
	var u entities.ResetPasswordInput

	{
		u.IdentifierKey = c.GetHeader(os.Getenv("IDENTITY_KEY_HEADER"))
		u.Id = c.GetHeader(os.Getenv("USER_ID_HEADER"))
	}

	{
		validationChain(c, h.validator, h.policyManager, &u)
	}

	err := h.service.ResetPassword(u)

	{
		if err != nil {
			newErrorResponse(c, http.StatusFailedDependency, err.Error())
			return
		}
	}

	{
		c.JSON(http.StatusResetContent, responses.ResetPasswordResponse{
			StatusCode: http.StatusResetContent,
			Message:    responses.ResetPasswordSuccessMessage,
		})
	}

	{
		err := c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (h *Handler) socialAuthRedirect(c *gin.Context) {
	//c.Param("provider")
	//
	//var (
	//	githubConfig = &oauth2.Config{
	//		ClientID:     "",
	//		ClientSecret: "",
	//		Endpoint:     gitlab.Endpoint,
	//		RedirectURL:  "",
	//		Scopes:       nil,
	//	}
	//gitlabConfig = &oauth2.Config{
	//ClientID:     "",
	//ClientSecret: "",
	//Endpoint:     github.Endpoint,
	//RedirectURL:  "",
	//Scopes:       nil,
	//}
	//
	//googleConfig = &oauth2.Config{
	//ClientID:     "",
	//ClientSecret: "",
	//Endpoint:     google.Endpoint,
	//RedirectURL:  "",
	//Scopes:       []string{"https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"},
	//}
	//
	//facebookConfig = &oauth2.Config{
	//ClientID:     "",
	//ClientSecret: "",
	//Endpoint:     facebook.Endpoint,
	//RedirectURL:  "",
	//Scopes:       nil,
	//}
	//)
	//
	//if githubConfig == nil {
	//	newErrorResponse(c, http.StatusBadRequest, "Invalid Authentication Provider")
	//}
	//
	//authURL := githubConfig.AuthCodeURL("State", oauth2.AccessTypeOnline)
	//c.Redirect(http.StatusFound, authURL)
	//
	//{
	//	err := c.Request.Body.Close()
	//	if err != nil {
	//		log.Println(err.Error())
	//	}
	//}
}

func (h *Handler) socialAuthCallback(c *gin.Context) {
	{
		err := c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (h *Handler) verifyUser(c *gin.Context) {
	{
		err := c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (h *Handler) invalidateUserSession(c *gin.Context) {
	{
		err := c.Request.Body.Close()
		if err != nil {
			log.Println(err.Error())
		}
	}
}
