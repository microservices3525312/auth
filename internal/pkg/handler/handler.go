package handler

import (
	"os"

	"github.com/didip/tollbooth/v6/limiter"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	_ "gitlab.com/microservices3525312/auth/cmd/docs"
	"gitlab.com/microservices3525312/auth/internal/pkg/service"
	helperTools "gitlab.com/microservices3525312/tools/microhelpers/microHelpers"
)

//go:generate mockery --name=Handler
type Handler struct {
	service       *service.Service
	policyManager *helperTools.PolicyManager
	rateLimiter   *limiter.Limiter
	validator     *validator.Validate
}

func New(s *service.Service, pM *helperTools.PolicyManager, rL *limiter.Limiter, v *validator.Validate) *Handler {
	return &Handler{service: s, policyManager: pM, rateLimiter: rL, validator: v}
}

func (h *Handler) DefineRoutes() *gin.Engine {
	r := gin.New()

	r.Use(h.customLogger(), h.requestLimiter(h.rateLimiter))

	{
		corsConfig := cors.DefaultConfig()
		corsConfig.AllowOrigins = []string{"localhost:4321"}
		corsConfig.AllowMethods = []string{"GET", "POST", "PUT", "DELETE"}
		corsConfig.AllowHeaders = []string{"Origin", "Content-Type", "Authorization", os.Getenv("IDENTITY_KEY_HEADER")}
	}
	{
		docs(r, h)
	}

	{
		registration(r, h)
		authentication(r, h)
		socialNetworkAuth(r, h)
		passwordManagement(r, h)
		logout(r, h)
	}

	return r
}
