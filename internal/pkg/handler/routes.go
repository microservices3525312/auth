package handler

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func docs(r *gin.Engine, h *Handler) {
	docs := r.Group("/docs")
	{
		docs.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}
}

func registration(r *gin.Engine, h *Handler) {
	registration := r.Group("api/authServer/registration")
	{
		registration.POST("/create-user", h.requestSizeLimiter(h.createUser, 46, 1024))
		registration.POST("/verify-user", h.verifyUser)
	}
}

func authentication(r *gin.Engine, h *Handler) {
	authentication := r.Group("api/authServer/authentication")
	{
		authentication.POST("/login-user", h.requestSizeLimiter(h.loginUser, 30, 476))
		authentication.POST("/refresh-login", h.refreshLogin)
	}
}

func socialNetworkAuth(r *gin.Engine, h *Handler) {
	socialNetworkAuth := r.Group("api/authServer/socialNetworkAuth")
	{
		socialNetworkAuth.GET("/:provider/redirect", h.socialAuthRedirect)
		socialNetworkAuth.GET("/:provider/callback", h.socialAuthCallback)
	}
}

func passwordManagement(r *gin.Engine, h *Handler) {
	passwordManagement := r.Group("/api/authServer/passwordManagement")
	{
		passwordManagement.POST("/request-password-reset", h.requestSizeLimiter(h.requestResetPassword, 33, 516))
		passwordManagement.POST("/reset-password", h.requestSizeLimiter(h.resetPassword, 30, 514))
	}
}

func logout(r *gin.Engine, h *Handler) {

	logout := r.Group("/api/authServer/logout")
	{
		logout.POST("/invalidate-user-session", h.invalidateUserSession)
	}
}
