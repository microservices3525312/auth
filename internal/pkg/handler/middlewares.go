package handler

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"io"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/didip/tollbooth/v6"
	"github.com/didip/tollbooth/v6/limiter"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/microservices3525312/auth/internal/entities"
	"gitlab.com/microservices3525312/auth/internal/responses"
	"gitlab.com/microservices3525312/tools/microhelpers/microHelpers"
)

import (
	"fmt"
)

type ValidationObjects interface {
	entities.User | entities.UserInput | entities.RecoverPasswordInput | entities.ResetPasswordInput
}

type CustomLogger struct {
	mu     sync.Mutex
	logger *log.Logger
}

func NewCustomLogger() *CustomLogger {
	return &CustomLogger{logger: log.New(os.Stdout, "", log.LstdFlags)}
}

func (cl *CustomLogger) Log(c *gin.Context, start time.Time, statusCode int, size int) {
	cl.mu.Lock()
	defer cl.mu.Unlock()

	cl.logger.Printf("[Request - %s] -> Path: [%s] -> Date [%s] -> Latency: [%d ms] -> StatusCode: [%d] -> [%f KB]\n",
		c.Request.URL.Path,
		time.Now().Format("2 January 2006 : 15:04:05:00"),
		time.Since(start).Milliseconds(),
		statusCode,
		float64(size)/1024,
	)
}

func (h *Handler) logger(customLogger *CustomLogger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		customLogger.Log(c, time.Now(), c.Writer.Status(), c.Writer.Size())
	}
}

const (
	brightGreen  = "\033[38;5;10m"
	brightYellow = "\033[38;5;11m"
	brightBlue   = "\033[38;5;12m"
	brightOrange = "[38;5;208m"
	brightPink   = "[38;5;207m"
	reset        = "[0m"

	reqIdLength = 5

	separator = "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
)

func (h *Handler) customLogger() gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}

func generateShortUUID() string {
	randBytes := make([]byte, reqIdLength)
	_, err := rand.Read(randBytes)
	if err != nil {
		return ""
	}
	return hex.EncodeToString(randBytes)
}

func (h *Handler) requestLimiter(l *limiter.Limiter) gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := tollbooth.LimitByRequest(l, c.Writer, c.Request); err != nil {
			newErrorResponse(c, http.StatusTooManyRequests, fmt.Sprintf("Too many requests: \n%s", err.Error()))
			return
		}
		c.Next()
	}
}

func (h *Handler) requestSizeLimiter(handlerFunc gin.HandlerFunc, minReqBodySize, maxReqBodySize int16) gin.HandlerFunc {
	return func(c *gin.Context) {
		body, err := io.ReadAll(c.Request.Body)
		if err != nil {
			newErrorResponse(c, http.StatusInternalServerError, err.Error())
			return
		}

		if err := helperTools.LimitReqBody(body, 1200); err != nil {
			newErrorResponse(c, http.StatusRequestEntityTooLarge, err.Error())
			return
		}

		c.Request.Body = io.NopCloser(bytes.NewBuffer(body))

		handlerFunc(c)
	}
}

func validationChain[K ValidationObjects](c *gin.Context, validator *validator.Validate, pM *helperTools.PolicyManager, objectToValidate *K) {
	if err := c.BindJSON(objectToValidate); err != nil {
		newErrorResponse(c, http.StatusBadRequest, responses.BadRequestErrorMessage)
		return
	}

	err := helperTools.XSSInput(objectToValidate, pM)
	if err != nil {
		newErrorResponse(c, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if err := validator.Struct(objectToValidate); err != nil {
		newErrorResponse(c, http.StatusUnprocessableEntity, fmt.Sprintf("%s", helperTools.GenerateValidationStruct(err)))
		return
	}
}
