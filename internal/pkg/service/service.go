package service

import (
	"github.com/go-redis/redis/v8"
	mq "github.com/rabbitmq/amqp091-go"
	"gitlab.com/microservices3525312/auth/internal/entities"
	"gitlab.com/microservices3525312/auth/internal/pkg/repository"
)

//go:generate mockgen -source=service.go -destination=mocks/mock.go
type Authorizer interface {
	CreateUser(u *entities.User) (entities.AuthenticatedUserResponse, error)
	VerifyUser()
	LoginUser(u entities.UserInput) (entities.AuthenticatedUserResponse, error)
	RefreshLogin(u entities.RefreshLoginEntity) (entities.AuthenticatedUserResponse, error)
	Redirect()
	Callback()
	RequestPasswordRecover(u *entities.RecoverPasswordInput) error
	ResetPassword(u entities.ResetPasswordInput) error
	InvalidateSession()
}

type Service struct {
	Authorizer
}

type AuthService struct {
	repo         repository.AuthDB
	messageQueue *mq.Connection
	redis        *redis.Client
}

func newAuthService(repo repository.Repository, mq *mq.Connection, r *redis.Client) *AuthService {
	return &AuthService{repo: repo, messageQueue: mq, redis: r}
}

func New(repository repository.Repository, messageQueue *mq.Connection, redis *redis.Client) *Service {
	return &Service{Authorizer: newAuthService(repository, messageQueue, redis)}
}
