// Code generated by MockGen. DO NOT EDIT.
// Source: service.go

// Package mock_service is a generated GoMock package.
package mock_service

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	entities "gitlab.com/microservices3525312/auth/internal/entities"
)

// MockAuthorizer is a mock of Authorizer interface.
type MockAuthorizer struct {
	ctrl     *gomock.Controller
	recorder *MockAuthorizerMockRecorder
}

// MockAuthorizerMockRecorder is the mock recorder for MockAuthorizer.
type MockAuthorizerMockRecorder struct {
	mock *MockAuthorizer
}

// NewMockAuthorizer creates a new mock instance.
func NewMockAuthorizer(ctrl *gomock.Controller) *MockAuthorizer {
	mock := &MockAuthorizer{ctrl: ctrl}
	mock.recorder = &MockAuthorizerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAuthorizer) EXPECT() *MockAuthorizerMockRecorder {
	return m.recorder
}

// Callback mocks base method.
func (m *MockAuthorizer) Callback() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Callback")
}

// Callback indicates an expected call of Callback.
func (mr *MockAuthorizerMockRecorder) Callback() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Callback", reflect.TypeOf((*MockAuthorizer)(nil).Callback))
}

// CreateUser mocks base method.
func (m *MockAuthorizer) CreateUser(u *entities.User) (entities.AuthenticatedUserResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateUser", u)
	ret0, _ := ret[0].(entities.AuthenticatedUserResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateUser indicates an expected call of CreateUser.
func (mr *MockAuthorizerMockRecorder) CreateUser(u interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateUser", reflect.TypeOf((*MockAuthorizer)(nil).CreateUser), u)
}

// InvalidateSession mocks base method.
func (m *MockAuthorizer) InvalidateSession() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "InvalidateSession")
}

// InvalidateSession indicates an expected call of InvalidateSession.
func (mr *MockAuthorizerMockRecorder) InvalidateSession() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InvalidateSession", reflect.TypeOf((*MockAuthorizer)(nil).InvalidateSession))
}

// LoginUser mocks base method.
func (m *MockAuthorizer) LoginUser(u entities.UserInput) (entities.AuthenticatedUserResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "LoginUser", u)
	ret0, _ := ret[0].(entities.AuthenticatedUserResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// LoginUser indicates an expected call of LoginUser.
func (mr *MockAuthorizerMockRecorder) LoginUser(u interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "LoginUser", reflect.TypeOf((*MockAuthorizer)(nil).LoginUser), u)
}

// Redirect mocks base method.
func (m *MockAuthorizer) Redirect() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Redirect")
}

// Redirect indicates an expected call of Redirect.
func (mr *MockAuthorizerMockRecorder) Redirect() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Redirect", reflect.TypeOf((*MockAuthorizer)(nil).Redirect))
}

// RequestPasswordRecover mocks base method.
func (m *MockAuthorizer) RequestPasswordRecover(u *entities.RecoverPasswordInput) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RequestPasswordRecover", u)
	ret0, _ := ret[0].(error)
	return ret0
}

// RequestPasswordRecover indicates an expected call of RequestPasswordRecover.
func (mr *MockAuthorizerMockRecorder) RequestPasswordRecover(u interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RequestPasswordRecover", reflect.TypeOf((*MockAuthorizer)(nil).RequestPasswordRecover), u)
}

// ResetPassword mocks base method.
func (m *MockAuthorizer) ResetPassword(u *entities.RecoverPasswordInput) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ResetPassword", u)
	ret0, _ := ret[0].(error)
	return ret0
}

// ResetPassword indicates an expected call of ResetPassword.
func (mr *MockAuthorizerMockRecorder) ResetPassword(u interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ResetPassword", reflect.TypeOf((*MockAuthorizer)(nil).ResetPassword), u)
}

// VerifyUser mocks base method.
func (m *MockAuthorizer) VerifyUser() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "VerifyUser")
}

// VerifyUser indicates an expected call of VerifyUser.
func (mr *MockAuthorizerMockRecorder) VerifyUser() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "VerifyUser", reflect.TypeOf((*MockAuthorizer)(nil).VerifyUser))
}
