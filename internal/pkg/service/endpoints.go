package service

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	mq "github.com/rabbitmq/amqp091-go"
	"gitlab.com/microservices3525312/auth/internal/entities"
	helperTools "gitlab.com/microservices3525312/tools/microhelpers/microHelpers"
)

func (a *AuthService) CreateUser(u *entities.User) (entities.AuthenticatedUserResponse, error) {
	salt, err := helperTools.Salt(91)
	if err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}

	{
		hashedPass, err := helperTools.Hash(u.Password, u.UserIdentifierKey)
		if err != nil {
			return entities.AuthenticatedUserResponse{}, err
		}

		{
			u.Password = hashedPass
			u.UserIdentifierKey = salt
			u.IPAddress = helperTools.GetIPv6()
			u.DateCreated = helperTools.GetFormattedDateTime()
		}
	}

	{
		id, err := a.repo.CreateUser(u)
		a.redis.Set(context.Background(), fmt.Sprintf("Key Of %s", id), salt, 0)

		if err != nil {
			return entities.AuthenticatedUserResponse{}, err
		}

		aT, err := helperTools.GenerateToken(id, os.Getenv("TOKEN_EXPIRY_TIME"), "access", []byte(u.UserIdentifierKey))
		if err != nil {
			return entities.AuthenticatedUserResponse{}, err
		}

		rT, err := helperTools.GenerateToken(id, os.Getenv("TOKEN_EXPIRY_TIME"), "refresh", []byte(u.UserIdentifierKey))
		if err != nil {
			return entities.AuthenticatedUserResponse{}, err
		}

		return entities.AuthenticatedUserResponse{
			UserID:                id,
			AccessToken:           aT,
			AccessTokenExpiresAt:  fmt.Sprintf("%s Minutes", os.Getenv("TOKEN_EXPIRY_TIME")),
			RefreshToken:          rT,
			RefreshTokenExpiresAt: "13 days",
		}, nil
	}
}

func (a *AuthService) LoginUser(u entities.UserInput) (entities.AuthenticatedUserResponse, error) {
	userInfo, err := a.repo.LoginUser(u)
	if err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}

	hashedPass, err := helperTools.Hash(u.Password, u.UserIdentifierKey)
	if err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}
	{
		u.UserIdentifierKey, err = a.redis.Get(context.Background(), fmt.Sprintf("Key Of %s", userInfo.UserID)).Result()
		if err != nil {
			return entities.AuthenticatedUserResponse{}, errors.New("no keys for this user in db")
		}
	}

	{

		if userInfo.Password == hashedPass {
			aT, err := helperTools.GenerateToken(userInfo.UserID, os.Getenv("TOKEN_EXPIRY_TIME"), "access", []byte(u.UserIdentifierKey))
			if err != nil {
				return entities.AuthenticatedUserResponse{}, err
			}

			rT, err := helperTools.GenerateToken(userInfo.UserID, os.Getenv("TOKEN_EXPIRY_TIME"), "refresh", []byte(u.UserIdentifierKey))
			if err != nil {
				return entities.AuthenticatedUserResponse{}, err
			}

			return entities.AuthenticatedUserResponse{
				UserID:                userInfo.UserID,
				AccessToken:           aT,
				AccessTokenExpiresAt:  fmt.Sprintf("%s Minutes", os.Getenv("TOKEN_EXPIRY_TIME")),
				RefreshToken:          rT,
				RefreshTokenExpiresAt: "13 days",
			}, nil
		}
		err = errors.New("user not found")
	}

	return entities.AuthenticatedUserResponse{}, err
}

func (a *AuthService) RequestPasswordRecover(u *entities.RecoverPasswordInput) error {
	id, err := a.repo.RequestPasswordRecover(u)
	isUUID := helperTools.CheckUUID(id)

	{
		ch, err := a.messageQueue.Channel()
		if err != nil {
			return err
		}
		defer func() {
			err := ch.Close()
			if err != nil {
				return
			}
		}()
		otp, err := helperTools.OTP()
		fmt.Println(otp, "otp")
		if err != nil {
			return err
		}

		{
			a.redis.Set(context.Background(), fmt.Sprintf("OTP for %s", id), otp, 1*time.Minute)
		}

		{
			if err := ch.PublishWithContext(
				context.Background(),
				"auth_exchange",
				"auth",
				false, false,
				mq.Publishing{
					ContentType: "text/plain",
					Body:        []byte(otp),
				},
			); err != nil {
				return err
			}
		}

	}

	{
		if err != nil || (err != nil && !isUUID) {
			return err
		}
	}

	return nil
}

func (a *AuthService) ResetPassword(u entities.ResetPasswordInput) error {
	key, err := a.redis.Get(context.Background(), fmt.Sprintf("Key Of %s", u.Id)).Result()
	if err != nil {
		return errors.New("no userID found")
	}

	{
		otp, err := a.redis.Get(context.Background(), fmt.Sprintf("OTP for %s", u.Id)).Result()
		if u.Code != otp && err != nil {
			return errors.New("invalid OTP or OTP expired and deleted")
		}

		a.redis.Del(context.Background(), fmt.Sprintf("OTP for %s", u.Id))
	}

	{
		hashedPass, err := helperTools.Hash(u.NewPassword, key)
		if err != nil {
			return err
		}
		u.NewPassword = hashedPass
	}

	return a.repo.ResetPassword(u)
}

func (a *AuthService) VerifyUser() {
}

func (a *AuthService) Redirect() {
}

func (a *AuthService) Callback() {
}

func (a *AuthService) InvalidateSession() {
}

func (a *AuthService) RefreshLogin(u entities.RefreshLoginEntity) (entities.AuthenticatedUserResponse, error) {
	salt, err := a.redis.Get(context.Background(), fmt.Sprintf("Key Of %s", u.UserID)).Result()
	if err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}

	token, err := helperTools.VerifyToken(u.RefreshHeader, []byte(salt))
	if !token.Valid && err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}

	aT, err := helperTools.GenerateToken(u.UserID, os.Getenv("TOKEN_EXPIRY_TIME"), "access", []byte(salt))
	if err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}

	rT, err := helperTools.GenerateToken(u.UserID, os.Getenv("TOKEN_EXPIRY_TIME"), "refresh", []byte(salt))
	if err != nil {
		return entities.AuthenticatedUserResponse{}, err
	}

	return entities.AuthenticatedUserResponse{
		UserID:                u.UserID,
		AccessToken:           aT,
		AccessTokenExpiresAt:  os.Getenv("TOKEN_EXPIRY_TIME"),
		RefreshToken:          rT,
		RefreshTokenExpiresAt: "13 Days",
	}, nil
}
