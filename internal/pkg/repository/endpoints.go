package repository

import (
	"context"

	"gitlab.com/microservices3525312/auth/internal/entities"
	"gitlab.com/microservices3525312/auth/internal/queries"
)

func (a *AuthPostgres) CreateUser(u *entities.User) (string, error) {
	var userID string

	row := a.db.QueryRow(context.Background(), queries.CreateUserQuery,
		u.Name, u.Surname, u.UserName, u.Email, u.TelNumber,
		u.IDNumber, u.Password, u.DateCreated, u.IPAddress, u.AutoLogoutTime,
	)

	err := row.Scan(&userID)
	{
		if err != nil {
			return "", err
		}
	}

	return userID, nil
}

func (a *AuthPostgres) LoginUser(u entities.UserInput) (entities.UserMetaInfo, error) {
	var userID string
	var password string

	row := a.db.QueryRow(context.Background(), queries.LoginUserQuery, u.Email, u.IDNumber)

	if err := row.Scan(&userID, &password); err != nil {
		return entities.UserMetaInfo{}, err
	}

	return entities.UserMetaInfo{
		Password: password,
		UserID:   userID,
	}, nil
}

func (a *AuthPostgres) RequestPasswordRecover(u *entities.RecoverPasswordInput) (string, error) {
	var userID string

	row := a.db.QueryRow(context.Background(), queries.CheckUserExistenceQuery, u.Email, u.IDNumber, u.TelNumber)

	err := row.Scan(&userID)
	if err != nil {
		return "", err
	}

	return userID, nil
}

func (a *AuthPostgres) ResetPassword(u entities.ResetPasswordInput) error {
	var userID string

	row := a.db.QueryRow(context.Background(), queries.ResetPasswordQuery, u.NewPassword, u.Email, u.TelNumber)

	return row.Scan(&userID)
}

func (a *AuthPostgres) VerifyUser() {}

func (a *AuthPostgres) Redirect() {}

func (a *AuthPostgres) Callback() {}

func (a *AuthPostgres) InvalidateSession() {}
