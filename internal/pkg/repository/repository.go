package repository

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/microservices3525312/auth/internal/entities"
)

//go:generate mockery --name=AuthDB
type AuthDB interface {
	CreateUser(u *entities.User) (string, error)
	VerifyUser()
	LoginUser(u entities.UserInput) (entities.UserMetaInfo, error)
	Redirect()
	Callback()
	RequestPasswordRecover(u *entities.RecoverPasswordInput) (string, error)
	ResetPassword(u entities.ResetPasswordInput) error
	InvalidateSession()
}

type Repository struct {
	AuthDB
}

type AuthPostgres struct {
	db *pgxpool.Pool
}

func NewAuthPostgres(db *pgxpool.Pool) *AuthPostgres {
	return &AuthPostgres{db: db}
}

func New(db *pgxpool.Pool) Repository {
	return Repository{AuthDB: NewAuthPostgres(db)}
}
