package queries

const (
	CreateUserQuery         = `INSERT INTO users (name, surname, username, email, tel_number,  id_number, password, date_created, ip_address, auto_logout_time) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING user_id`
	LoginUserQuery          = `SELECT user_id, password from users where email = $1 and id_number = $2`
	CheckUserExistenceQuery = `SELECT user_id FROM users WHERE email = $1 and id_number = $2 and tel_number = $3`
	ResetPasswordQuery      = "UPDATE users SET password = $1 where email = $2 and tel_number = $3 RETURNING user_id"
)
