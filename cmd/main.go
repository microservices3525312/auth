package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/didip/tollbooth/v6"
	"github.com/go-playground/validator/v10"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/microservices3525312/auth/internal/entities"
	"gitlab.com/microservices3525312/auth/internal/outerServices"
	"gitlab.com/microservices3525312/auth/internal/pkg/handler"
	"gitlab.com/microservices3525312/auth/internal/pkg/repository"
	"gitlab.com/microservices3525312/auth/internal/pkg/service"
	helperTools "gitlab.com/microservices3525312/tools/microhelpers/microHelpers"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		logrus.Fatalf("Failed to load environment variables: %s", err.Error())
	}

	rateLimiter := tollbooth.NewLimiter(1, nil)

	pm := new(helperTools.PolicyManager)

	logrus.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat:   "18:08:20",
		DisableTimestamp:  false,
		DisableHTMLEscape: false,
		FieldMap:          nil,
		CallerPrettyfier:  nil,
		PrettyPrint:       true,
	})

	dbConn, err := pgxpool.Connect(context.Background(), os.Getenv("DB_CONN_STRING"))
	if err != nil {
		log.Fatal(err)
	}

	validator := validator.New()
	mqConn, err := outerServices.MQ(os.Getenv("RABBITMQ_CONN"))
	if err != nil {
		log.Fatal(err)
	}

	redis := outerServices.RedisClient()

	outerServices.AuthQueue(mqConn)

	repos := repository.New(dbConn)
	services := service.New(repos, mqConn, redis)
	h := handler.New(services, pm, rateLimiter, validator)

	s := new(entities.Server)

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)

	go func() {
		if err := s.Run(os.Getenv("AUTH_SERVER_PORT"), h.DefineRoutes()); err != nil {
			logrus.Fatalf("Error occurred while initializing server: %s", err.Error())
		}
	}()

	<-signalChan

	if err := mqConn.Close(); err != nil {
		fmt.Println("Failed to close RabbitMQ Connection")
	}
	dbConn.Close()

	log.Println("All Connections Closed")
}
