# Use the official Golang image as the base image
FROM golang:1.20 AS builder

# Set the working directory
WORKDIR /app

# Copy go.mod, go.sum, and .env to the working directory
COPY go.mod go.sum .env ./

# Install the dependencies
RUN go mod download

# Copy the source code to the working directory
COPY . .

RUN apt-get update
RUN apt-get -y install postgresql-client

RUN chmod +x postgres-compose.sh

# Build the application
RUN CGO_ENABLED=0 GOOS=linux go build -o /app/main ./cmd/main.go

# Use the distroless base image for the runtime environment
FROM gcr.io/distroless/base

# Set the working directory
WORKDIR /app

# Copy the .env file from the builder stage
COPY --from=builder /app/.env /app/.env

# Copy the binary from the builder stage to the runtime stage
COPY --from=builder /app/main /app/main

# Expose the port your microservice listens on
EXPOSE 8080

# Start the microservice with HTTPS and pass the certificate and key files as flags
CMD [ "/app/main", "-https", "-cert", "/app/cert.pem", "-key", "/app/key.pem" ]
